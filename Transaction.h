#ifndef TRANSACTION_H
#define TRANSACTION_H

namespace block_ns {

  class BlockChain;

  class Transaction
  {

    public:
      //Crypto Addresses of sender and receiver
      std::string from;
      std::string to;
      double value;
      bool successful_transaction;
      std::vector<uint8_t> sig;

      Transaction();
      Transaction(const std::string& sender, const std::string& receiver, const double& amount, block_ns::BlockChain* chain);

  private:
    bool update_blockchain(block_ns::BlockChain* chain);

  };

  }

#endif
