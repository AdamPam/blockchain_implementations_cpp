#ifndef BLOCK_H
#define BLOCK_H

#include <botan/hash.h>
#include <botan/hex.h>
#include <botan/auto_rng.h>
#include <botan/ecdsa.h>
#include <botan/ec_group.h>
#include <botan/pubkey.h>
#include <botan/hex.h>

namespace block_ns {

  class Block
  {
    public:
      Botan::secure_vector<uint8_t> curr_hash; /* I would use vector for binary data (a hash) */
      std::vector<block_ns::Transaction> contents;
      int block_number;
      Botan::secure_vector<uint8_t> next_Block;
      Botan::secure_vector<uint8_t> prev_Block;
      int nonce = 0;

      /* Explicitly marking the default constructor instead of just putting {} */
      Block() = default;

      //Parameterised Constructors (genesis block and normal block)
      /* There is something in the standard that says if you have a single parameter for a constructor it should be marked as explicit */
      explicit Block(const std::vector<block_ns::Transaction>& data);
      /**
       * **Important**
       *
       * I have changed the order in which you inline construct you member variables.
       *
       * You should **ALWAYS** initialise in the order they appear in the class. You had them out of order.
       *
       * This doesn't throw a complier error because the compiler actually changes the order for you (stupidly) and wont throw an error.
       * Some static analyses will throw an error if they can find it (g++ also finds it on mine because I have warnings level on perdentic).
       *
       * This may seem trivial (and it is in your case) and many people ignore it cause the compiler does stop it
       * This leads to massive undetectable bugs in code and happens all the time.
       *
       * Ill show you an example which leads to undefined behaviour but wont effect program execution 99% of the time.
       *

      class buggy_vec {
        size_t vec_size_;
        std::vector<uint8_t> vec_;

        explicit buggy_vec(const std::vector<uint8_t>& _vec)
          :	vec_(_vec), vec_size_(vec_.size())
          {}
      };

  */
      /**
       * Can you see the bug here?
       *
       * vec_size_ is acutally constructed before vec_.
       *
       * but vec_size_ is using a member of vec_. So you dereference an object that is not actually constructed.
       *
       * this is undefined behaviour.
       *
       */
      Block(const std::vector<Transaction>& data, const Botan::secure_vector<uint8_t>& prev_hash, int block_num);

      //Copy constructor
      /* Although its good practice for c++, we don't actually need a copy constructor here as default will do */
      Block(const Block &b);

      /* Ill do a neive one for you */
      /* equal operator should return a reference to the object in question */
      /* this allows for this notation, Block a = b = c; */

      Block& operator=(const Block& b);

  };

}

#endif
