#ifndef NODE_H
#define NODE_H

#include <botan/hash.h>
#include <botan/hex.h>
#include <botan/auto_rng.h>
#include <botan/ecdsa.h>
#include <botan/ec_group.h>
#include <botan/pubkey.h>
#include <botan/hex.h>


#ifndef CRYPTO_SETUP
#define CRYPTO_SETUP

const std::string HASH_ALGO = "SHA-256";
const std::string EMSA = "EMSA1(SHA-256)";
const std::string CURVE = "secp521r1";
Botan::AutoSeeded_RNG rng;

#endif

namespace block_ns {

  class Node
  {
    //A node holds a key pair and address.
    //Creating signatures is done from a Node instance, verifying signatures is done from a static method in the Node class
    private:
      Botan::ECDSA_PrivateKey key{rng, Botan::EC_Group(CURVE)};
    public:
      std::string address;

      Node();

      std::vector<uint8_t> create_ECC_signature(const std::string& text);

      Botan::ECDSA_PublicKey get_public_key();

      //This can be static as we pass it a public key i.e. it does not need access to an underlying Node with a key
  };

  }

#endif
