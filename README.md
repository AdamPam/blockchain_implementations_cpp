This is a basic C++ blockchain implementation with a Bitcoin-style PoW consenus algorithm.

_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
		INSTRUCTIONS
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _

		Unix-Style

NOTE: To compile on your system, you need the Botan C++ cryptography library and the openSSL C++ cryptography library installed 
(check your package manager to see if you can install the libs easy this way)

Then, go into compile.sh and change the BOTAN_HEADERS and OPENSSL_LIB variables to the location of your Botan .h header files and your openssl/lib directory respectively. 

1. Compile with the following command: 
    sh compile.sh blockchain (or bash-x compile.sh blockchain, or chmod +x compile.sh, then ./compile.sh blockchain)
2. Run the executable with a parameter indicating PoW difficulty (num of prefix 0s in hash): 
    ./blockchain [PoW difficulty, recommended 1 or 2 for speed of execution)

You can play around with the constants NUM_TXS_IN_BLOCK and NUM_NODES to change blockchain parameters.
