#!/bin/bash

#This is for the #include </botan/...> commands
BOTAN_HEADERS="/usr/local/Cellar/botan/2.9.0/include/botan-2"
#This is so botan can use openssl libraries in the background
OPENSSL_LIB="/usr/local/opt/openssl/lib"

if [[ $# != 1 ]]
then
	echo "Usage ./compile.sh [file NOT INCLUDING .cpp]"
	exit 1
fi

g++ -std=c++11  $1.cpp -o $1 -I${BOTAN_HEADERS} -L${OPENSSL_LIB} -lbotan-2 -lbz2 -lcrypto -ldl -lz
