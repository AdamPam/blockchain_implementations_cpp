#ifndef BLOCKCHAIN_H
#define BLOCKCHAIN_H

#include <botan/hash.h>
#include <botan/hex.h>
#include <botan/auto_rng.h>
#include <botan/ecdsa.h>
#include <botan/ec_group.h>
#include <botan/pubkey.h>
#include <botan/hex.h>

namespace block_ns {

  class Block;

  class BlockChain
  {
    public:
      //Reference to the hash of the last block
      Botan::secure_vector<uint8_t> last_block;
      block_ns::Block* tail;
      //Hash map, <k,v> ---> <hash,block>
      std::unordered_map<std::string, Block> chain;
      //Mapping of hashed public key to node
      std::unordered_map<std::string,Node> lookup;
      //Mapping of hashed public key to amount owned by that public key
      std::unordered_map<std::string,double> ledger;



      explicit BlockChain(const std::vector<block_ns::Transaction>& genesis_content);

      void add_block(const std::vector<block_ns::Transaction>& data);

      void iterate_blockchain();

      std::string add_node(const double& starting_balance);

      void show_balances();

  };

}

#endif
