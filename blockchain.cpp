#include<iostream>
#include<fstream>
#include<sstream>
#include<string>
#include<cmath>
#include<vector>
#include<unordered_map>
#include<stack>
#include<random>
#include<math.h>
#include<map>
#include<sstream>
#include<iomanip>
#include <botan/hash.h>
#include <botan/hex.h>
#include <botan/auto_rng.h>
#include <botan/ecdsa.h>
#include <botan/ec_group.h>
#include <botan/pubkey.h>
#include <botan/hex.h>

//Custom header files
#include "Node.h"
#include "Transaction.h"
#include "Block.h"
#include "BlockChain.h"

	/* const Object& -> Object& -> const Object -> Object */

	/*
		NEXT STEPS:

		- CHANGE BLOCKS TO HOLD A LIST OF TRANSACTIONS RATHER THAN AN ARBITARY STRING
	*/
#ifndef CRYPTO_SETUP
#define CRYPTO_SETUP

const std::string HASH_ALGO = "SHA-256";
const std::string EMSA = "EMSA1(SHA-256)";
const std::string CURVE = "secp521r1";
Botan::AutoSeeded_RNG rng;

#endif

const int NUM_TXS_IN_BLOCK = 5;
const int NUM_NODES = 5;



namespace block_ns { /* no global namespace */

	//Number of 0s required in hash, PROOF OF WORK
	int DIFFICULTY;

	//Returns a secure_vector hash of the input string
	Botan::secure_vector<uint8_t> hash_str(const std::string& input_str)
	{
	        std::unique_ptr<Botan::HashFunction> hash(Botan::HashFunction::create(HASH_ALGO));
	        for(char c : input_str) {
	                hash->update(c);
	        }
	        return hash->final();
	}

  //Returns a block signature as a Botan secure vector
	Botan::secure_vector<uint8_t> block_signature(const int& block_num, const Botan::secure_vector<uint8_t>& prev_hash, const std::string& block_contents, const int& nonce)
	{
		std::string concatenated_string = std::to_string(block_num) + Botan::hex_encode(prev_hash) + block_contents + std::to_string(nonce);
		return hash_str(concatenated_string);
	}

	//Tests whether hash meets PoW constraints
	bool test_hash(const Botan::secure_vector<uint8_t>& hash)
	{
		//This conditional is needed to begin the mining process
		if(hash.empty())
			return false;
		//Boolean function to determine if hash can be accepted
		for(int i = 0; i < DIFFICULTY; i++) {
			if((int)hash[i] != 0) { /* note '0' is different to 0 */
				return false;
			}
		}
		return true;
	}

	//Mines a hash as per Bitcoin-style PoW and increments nonce of the block being mined
	Botan::secure_vector<uint8_t> mine_hash(int block_num, const Botan::secure_vector<uint8_t>& prev_hash, const std::vector<Transaction>& block_contents, int& nonce) {
		//Mines the block by incrementing the nonce until hash is acceptable
		//Notes: nonce is made a reference so we increment the actual variable in the block from this function
		Botan::secure_vector<uint8_t> attempted_hash;
		std::string contents_str;
		for(int i = 0; i < block_contents.size(); i++)
		{
			contents_str += block_contents[i].from + block_contents[i].to + std::to_string(block_contents[i].value);
		}
		while(!test_hash(attempted_hash))
		{
			nonce++;
			attempted_hash = block_signature(block_num, prev_hash, contents_str, nonce);
		}
		std::cout << "A nonce of value: " << nonce << " gave a hash of: " << Botan::hex_encode(attempted_hash) << std::endl;
		return attempted_hash;
	}

	//This can be static as we pass it a public key i.e. it does not need access to an underlying Node with a key
	bool verify_signature(const Botan::Public_Key& key, const std::string& text, const std::vector<uint8_t>& sig)
	{
			std::vector<uint8_t> data(text.data(), text.data() + text.length());
			Botan::PK_Verifier verifier(key,EMSA);
			//MALICIOUS ATTACK: uncomment below code to alter data
			//data[1] = 2;
			verifier.update(data);
			return verifier.check_signature(sig);
	}

	//Implementing Node class functions
			Node::Node()
			{
				//Gives a hash of the public key
				address = key.fingerprint_public();
			}

			std::vector<uint8_t> Node::create_ECC_signature(const std::string& text)
			{
				//Construct a vector representation of data by passing the start and end addresses of the string
				//to a std::vector constructor
				std::vector<uint8_t> data(text.data(), text.data() + text.length());
				Botan::PK_Signer signer(key, rng, EMSA);
				//sign the data
				signer.update(data);
				std::vector<uint8_t> signature = signer.signature(rng);
				return signature;
		 }

			Botan::ECDSA_PublicKey Node::get_public_key()
			{
				//QUESTION TO ZAC: As Botan uses inheritance for key pairs, is this the best way to
				//get a public key from a private key i.e. just return the key and cast it implicitly to a public key?

				return key; /* Yep! - Easy isnt it */
				/* Note that by returning a non-pointer type and non-reference type, you are actually "slicing" the object */
				/* which means you are removing any information about the private key object. */
			}


	//implementation of Transaction functions



			//QUESTION TO ZAC: Is this the correct way to pass the blockchain object to the transaction class?
			/*
			* yes and no...
			*
			** It is the correct way to do it, but it also does not make sense.
			*
			* It is safer to try and keep objects distinct from each others.
			* Whilst references are good, they denote a dependency and coupling between class which we want to stay away from.
			*
			* If the blockchain object is ever deleted before the transaction objects, this leads to undefined behavior.
			* Whilst it is easier to ensure in this small example, harder to ensure in bigger systems.
			*
			*  I suggest instead of having it as a member, have it as an argument. This means a transaction is independent of
			*  the blockchain.
			*
			**/

			//Constructor
			Transaction::Transaction(const std::string& sender, const std::string& receiver, const double& amount, BlockChain* chain)
				:
				from(sender),
				to(receiver),
				value(amount)//,
				//blockchain(chain)
			{
				std::cout << "\nSending " << amount << " from\n" << sender << "\nto\n" << receiver << std::endl;
				//create a signature (just signing the string representation of the tx amount for now)
				sig = chain->lookup[from].create_ECC_signature(from+to);
				//update blockchain, passing a reference to this transaction
				successful_transaction = update_blockchain(chain);
			}
	/*
		Also a like to private methods after the public ones :)

		Makes more sense from a design point of view.

	*/

	/*
		Transaction class holds from, to, amount and a digital signature.

		It will attempt to publish the changes to the blockchain after construction
	*/

		bool Transaction::update_blockchain(BlockChain* chain)
		{
			//This function true if transaction is valid

			//If balance is insufficient, return false
			if(chain->ledger[from] < value)
			{
				std::cout << "\nTX DECLINED: INSUFFICIENT BALANCE\n" << std::endl;
				return false;
			}

			//Is it safe to pass around a Botan Key object like this, considering the function will cast it into a public key?
			if(!verify_signature(chain->lookup[from].get_public_key(),from+to,sig))
			{
				std::cout << "\nTX DECLINED: SIGNATURE NOT VERIFIED\n" << std::endl;
				return false;
			}

			std::cout << "... TRANSACTION HAS BEEN ALLOWED ..." << std::endl;
			chain->ledger[from] -= value;
			chain->ledger[to] += value;
			return true;
		}



	//Implementing Block class functions


	    Block::Block(const std::vector<Transaction>& data)
	    	: curr_hash(mine_hash(0,Botan::secure_vector<uint8_t>(),data, nonce)), contents(data), block_number(0), next_Block(Botan::secure_vector<uint8_t>()), prev_Block(Botan::secure_vector<uint8_t>())
	    {
	      std::cout << "Genesis Block Constructor" << std::endl;
	    }

	    /**
	     * **Important**
	     *
	     * I have changed the order in which you inline construct you member variables.
	     *
	     * You should **ALWAYS** initialise in the order they appear in the class. You had them out of order.
	     *
	     * This doesn't throw a complier error because the compiler actually changes the order for you (stupidly) and wont throw an error.
	     * Some static analyses will throw an error if they can find it (g++ also finds it on mine because I have warnings level on perdentic).
	     *
	     * This may seem trivial (and it is in your case) and many people ignore it cause the compiler does stop it
	     * This leads to massive undetectable bugs in code and happens all the time.
	     *
	     * Ill show you an example which leads to undefined behaviour but wont effect program execution 99% of the time.
	     *

	    class buggy_vec {
	    	size_t vec_size_;
	    	std::vector<uint8_t> vec_;

	    	explicit buggy_vec(const std::vector<uint8_t>& _vec)
	    		:	vec_(_vec), vec_size_(vec_.size())
	    		{}
	    };

*/
	    /**
	     * Can you see the bug here?
	     *
	     * vec_size_ is acutally constructed before vec_.
	     *
	     * but vec_size_ is using a member of vec_. So you dereference an object that is not actually constructed.
	     *
	     * this is undefined behaviour.
	     *
	     */



	    Block::Block(const std::vector<Transaction>& data, const Botan::secure_vector<uint8_t>& prev_hash, int block_num)
	    :	contents(data),
	    	block_number(block_num),
	      next_Block(Botan::secure_vector<uint8_t>()),
	      prev_Block(prev_hash)
	    {
				std::cout << block_num << " Block Constructor" << std::endl;
				curr_hash = mine_hash(block_num, prev_hash, data,nonce);
			}

	    //Copy constructor
	    /* Although its good practice for c++, we don't actually need a copy constructor here as default will do */
	    Block::Block(const Block &b)
			 : curr_hash(b.curr_hash),
				 contents(b.contents),
				 block_number(b.block_number),
				 next_Block(b.next_Block),
				 prev_Block(b.prev_Block),
				 nonce(b.nonce)
			{}


			/* Ill do a neive one for you */
			/* equal operator should return a reference to the object in question */
			/* this allows for this notation, Block a = b = c; */
	     Block& Block::operator=(const Block& b) {
	       std::cout << "assignment operator called" << std::endl;
	       /* People like to test for self assignment - although some people say your shouldn't... */
	       if (this != &b ) { //if they are different objects in memory

	       	/* This is a direct copy of the copy constructor */
	       	/* people hate this, so there is a thing called the copy and swap method i suggest looking up */
	       	curr_hash = b.curr_hash;
		      contents = b.contents;
		      block_number = b.block_number;
		      next_Block = b.next_Block;
		      prev_Block = b.prev_Block;
		      nonce = b.nonce;

	       }

	       return *this;
	     }


	std::ostream& operator<<(std::ostream &strm, const Block &b) {
	  return strm << "Block number: " << b.block_number << "\n  Block Hash: " << Botan::hex_encode(b.curr_hash) << "\n previous block: " << Botan::hex_encode(b.prev_Block) << " \n and nonce: " << b.nonce;
	}

	//Implementation of Blockchain class functions



    BlockChain::BlockChain(const std::vector<Transaction>& genesis_content)
		{
			//Create genesis block
      Block genesis_block(genesis_content);
      last_block = genesis_block.curr_hash;
			//Use std::move to ensure only 1 genesis block is mined
      chain.emplace(std::make_pair(Botan::hex_encode(genesis_block.curr_hash), std::move(genesis_block)));
			tail = &chain[Botan::hex_encode(last_block)];
    }

    void BlockChain::add_block(const std::vector<block_ns::Transaction>& data) {
      //Create a new block
			std::cout << "Adding block number: " << tail->block_number + 1 << std::endl;
      Block b(data, tail->curr_hash, tail->block_number + 1);
			//Just keep a reference to the new block hash for ease
			Botan::secure_vector<uint8_t>& b_hash = b.curr_hash;
      //Add the block to the 'blockchain' map
      /* Use emplace and a move to stop double construction */
      chain[Botan::hex_encode(b_hash)] = b;
      //Update references
      tail->next_Block = b_hash;
      last_block = b_hash;
			tail = &chain[Botan::hex_encode(last_block)];
    }

    void BlockChain::iterate_blockchain() {
      //Backwards iterate through the chain, store each block in a stack and then pop each block off from the stack
      //The purpose of the stack is so we can print off blocks in the logical order, rather than backwards
      std::cout << "\n_________STATE OF BLOCKCHAIN_________\n" << std::endl;
      std::stack<Block> blocks;
      //We iterate from the last block, so put all blocks in a stack and pop them off in order
      Botan::secure_vector<uint8_t> previous_hash = last_block;
			//Genesis block's previous hash will be an empty secure_vector
			while(!previous_hash.empty()) {
        blocks.push(chain[Botan::hex_encode(previous_hash)]);
        previous_hash = chain[Botan::hex_encode(previous_hash)].prev_Block;
      }
      while(!blocks.empty()) {
        Block b = blocks.top();
        std::cout << b << std::endl;
        blocks.pop();
      }
    }

		std::string BlockChain::add_node(const double& starting_balance)
		{
			//returns the crypto address of a newly created node
			Node n;
			std::cout << "\nAdded a new crypto address of : " << n.address << " with a starting balance of: " << starting_balance << std::endl;
			lookup.emplace(std::make_pair(n.address,n));
			ledger.emplace(std::make_pair(n.address, starting_balance));
			return n.address;
		}

		void BlockChain::show_balances()
		{
			std::cout << "CRYPTO BALANCES:" << std::endl;
			for(std::unordered_map<std::string,double>::iterator it = ledger.begin(); it != ledger.end(); ++it)
			{
				std::cout << "	-> Address: " << it->first << " has balance: " << it->second << std::endl;
			}
			std::cout << std::endl;
		}



	};


std::vector<block_ns::Transaction> generate_network_transactions(block_ns::BlockChain *chain_ptr, std::vector<std::string> addresses)
{
	std::vector<block_ns::Transaction> transactions;
	while(transactions.size() < NUM_TXS_IN_BLOCK)
	{
		//selects 2 nodes at random
		int n1 = rng.next_byte() % addresses.size();
		int n2 = rng.next_byte() % addresses.size();

		//Just doing small transactions from 0-255
		//Random integer part of val
		int val_i = rng.next_byte();
		//Random remainder (decimal) of val
		double val_d = (double)(rng.next_byte() % 100) / 100.0;

		double val = val_i + val_d;
		block_ns::Transaction t(addresses[n1],addresses[n2],val,chain_ptr);
		if(t.successful_transaction)
		{
			std::cout << "Transaction successful: " << addresses[n1] << " -> " << val << " -> " << addresses[n2] << std::endl;
			transactions.push_back(t);
		}
		else
		{
			std::cout << "Transaction bounced: " << addresses[n1] << " -/> " << val << " -/> " << addresses[n2] << std::endl;
		}
	}
	return transactions;
}


int main(int argc, char** argv)
{
  if(argc != 2) {
    std::cout << "Usage: ./execute <difficulty_level>" << std::endl;
    exit(0);
  }

	//test block construction and mining
  block_ns::DIFFICULTY = std::stoi(argv[1]);
  std::cout << "\n\nEach block will need a find a hash with " << block_ns::DIFFICULTY << " prefix \'0\'s\n\n" << std::endl;
	//test transactions

	block_ns::BlockChain blockchain(std::vector<block_ns::Transaction>{});
	block_ns::BlockChain *blockchain_ptr = &blockchain;
	std::vector<std::string> addresses;
	std::cout << "____Initialising nodes in network____" << std::endl;
	while(addresses.size() < NUM_NODES)
	{
		addresses.push_back(blockchain.add_node(rng.next_byte()));
	}

	std::cout << "____Generating randomised transactions____" << std::endl;

	/*
	std::string address_1 = blockchain.add_node(100);
	std::string address_2 = blockchain.add_node(200);
	std::string address_3 = blockchain.add_node(300);
	std::vector<block_ns::Transaction> b1_content;
	block_ns::Transaction t1(address_1, address_2, 50, blockchain_ptr);
	block_ns::Transaction t2(address_3, address_2, 50, blockchain_ptr);
	b1_content.push_back(t1);
	b1_content.push_back(t2);
	blockchain.add_block(b1_content);
	*/
	int num_blocks_test = 1;
	for(int i = 0; i < num_blocks_test; i++)
	{
			std::cout << "____Adding block # " << i << std::endl;
			std::vector<block_ns::Transaction> txs = generate_network_transactions(blockchain_ptr,addresses);
			blockchain.add_block(txs);
			blockchain.iterate_blockchain();
			blockchain.show_balances();
	}
	//std::vector<block_ns::Transaction) txs_b1 = generate_network_transactions(blockchain_ptr,addresses);
	//blockchain.iterate_blockchain();


	//blockchain.show_balances();

  return 0;
}
